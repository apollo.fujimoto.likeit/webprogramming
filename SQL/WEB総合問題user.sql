CREATE TABLE user(
id SERIAL NOT NULL PRIMARY KEY UNIQUE, 
login_id varchar(255) NOT NULL,
name varchar(255) NOT NULL,
birth_date DATE NOT NULL,
password varchar(255) NOT NULL,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL
);
