<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報詳細参照</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
crossorigin="anonymous">
</head>
    <font size="+2">${userInfo.name}さん</font>
    <a href="LogoutServlet" style="margin-left: 1000px;">ログアウト</a>
    <h1 align="center">ユーザー情報詳細参照</h1>
<body>
    <form>
  <div class="form-group">
    <label for="loginID" style="margin-left: 450px; margin-top: 30px">ログインID</label>
    ${userDetail.loginId}
    </div>
  <div class="form-group">
      <label for="username" style="margin-left: 450px; align-center margin-top: 35px">ユーザー名</label>
     ${userDetail.name}
  <div class="form-group">
    <label for="birthday" style="margin-left: 450px; margin-top: 30px">生年月日</label>
    ${userDetail.birthDate}
    </div>
  <div class="form-group">
    <label for="submitdate" style="margin-left: 450px; margin-top: 30px">登録日時</label>
    ${userDetail.createDate}
    </div>
        <div class="form-group">
    <label for="updatedate" style="margin-left: 450px; margin-top: 30px">更新日時</label>
${userDetail.updateDate}
    </div>
      <a href="UserListServlet" style="margin-left: 1000px;" >戻る</a>
    </div>
    </form>
</body>
</html>