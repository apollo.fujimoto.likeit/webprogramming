<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー削除</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
crossorigin="anonymous">
</head>
    <font size="+2">${userInfo.name}さん</font>
    <a href="LogoutServlet" style="margin-left: 1000px;">ログアウト</a>
    <h1 align="center">ユーザー削除</h1>
<body>
    <form action="UserDeleteServlet" method="post">
    <input type="hidden" name="id" value="${deleteUser.id}">
    <font style="margin-left: 300px; margin-top: 40px;">ログインID : ${deleteUser.id }を本当に削除してもよろしいでしょうか。</font>
    <button type="submit" style="position: absolute; left: 54%; top: 100%/" class="btn btn-primary btn-lg">OK</button>
<a type="button" style="position: absolute; left: 60%; top: 100%/" class="btn btn-secondary btn-lg" href="UserListServlet">キャンセル</a>
</form>
</body>
</html>