<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー一覧</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
crossorigin="anonymous">
</head>
     <font size="+2">${userInfo.name}さん</font>
    <a href="LogoutServlet" style="margin-left: 1000px;" >ログアウト</a>
    <h1 align="center" >ユーザー一覧</h1>
    <body>
<form action= "UserListServlet" method="post">
  <div class="form-group">
    <label for="formGroupExampleInput" style="margin-left: 400px; margin-top: 50px;">ログインID</label>
    <input type="text"style="width: 400px;" id="formGroupExampleInputID" placeholder="IDを入力" name="loginId">
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput2" style="margin-left: 400px;">ユーザー名</label>
    <input type="text" style="width: 400px;"  id="formGroupExampleInputUserName" placeholder="ユーザー名を入力" name="name">
    </div>
    <div>
        <lable style="margin-left: 400px;">生年月日</lable>
    <input type="date" style="width:195px; margin-left: 15px;" name="startDate"> ~ <input type="date" style="width:195px; margin-left 100px" name="endDate">
    </div>
    <button type="submit"class="btn btn-primary"  style="margin-left: 900px; margin-top: 20px;" >検索</button>
 <div>
    <hr>
    <a href="UserRegisterServlet" style="margin-left: 1000px;">新規登録</a>
    </div>
        <table class="table" style="margin-top: 30px;">
  <thead>
    <tr style="background-color: darkgray;">
      <th scope="col">ログインID</th>
      <th scope="col">ユーザー名</th>
      <th scope="col">生年月日</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row"></th>
      <c:forEach var="user" items="${userList}" >
       <tr>
       <td>${user.loginId}</td>
       <td>${user.name}</td>
       <td>${user.birthDate}</td>
      <td>
<a type="button" style="margin-left: 400px" class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>

<c:if test="${userInfo.loginId == 'admin' or userInfo.loginId == user.loginId }">
<a type="button" class="btn btn-secondary" href="UserUpdateServlet?id=${user.id}">更新</a>
</c:if>
<c:if test="${userInfo.loginId == 'admin'}">
<a type="button" class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
</c:if>
</td>
    </tr>
    </c:forEach>

  </tbody>
</table>
</form>
    </body>
</html>

