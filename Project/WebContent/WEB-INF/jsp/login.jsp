<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
crossorigin="anonymous">
</head>
    <h1 align="center">ログイン画面</h1>
<body>
<div class="container">

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	</div>
 <form action="LoginServlet" method="post">
  <div class="form-group">
    <label for="exampleInputPassword1" style="margin-left: 450px; margin-top: 30px">ログインID</label>
    <input type="text" name="loginId" style="width:400px;"id="exampleInputId" aria-describedby="emailHelp" placeholder="IDを入力">
    </div>
  <div class="form-group">
    <label for="exampleInputPassword1" style="margin-left: 450px;">パスワード</label>
    <input type="Password" name="password" style="width:400px;" id="exampleInputPassword1" placeholder="パスワードを入力">
    </div>
<button type="submit" class="btn btn-primary" style="margin-left: 675px; margin-top: 30px:">ログイン</button>
    </form>
    </body>
</html>