<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー登録</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
crossorigin="anonymous">
</head>
     <font size="+2">${userInfo.name}さん</font>
    <a href="LogoutServlet" style="margin-left: 1000px;">ログアウト</a>
    <h1 align="center">ユーザー登録</h1>
<body>
    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
    <form action="UserRegisterServlet" method="post" >
  <div class="form-group">
    <label for="exampleInputPassword1" style="margin-left: 450px; margin-top: 30px">ログインID</label>
    <input type="text" style="width:400px;"id="exampleInputID" aria-describedby="emailHelp" placeholder="IDを入力" name="loginId">
    </div>
  <div class="form-group">
    <label for="Password1" style="margin-left: 450px; margin-top: 30px">パスワード</label>
    <input type="Password" style="width:400px;" id="exampleInputPassword" placeholder="パスワードを入力" name="password">
    </div>
  <div class="form-group">
    <label for="Passwordcheck" style="margin-left: 450px; margin-top: 30px">パスワード確認
    </label>
    <input type="Password" style="width:370px;"id="exampleInputPassword1" aria-describedby="emailHelp" placeholder="パスワードを入力" name="checkPassword">
    </div>
  <div class="form-group">
    <label for="userName" style="margin-left: 450px; margin-top: 30px">ユーザー名</label>
    <input type="text" style="width:403px;" id="exampleInputUserName" placeholder="ユーザー名を入力" name="name">
    </div>
        <div class="form-group">
    <label for="userName" style="margin-left: 450px; margin-top: 30px">生年月日</label>
    <input type="date" style="width:420px;" id="exampleInputBirthDay" placeholder="生年月日を入力" name="birthDate">
    </div>
<button type="submit" class="btn btn-primary" style="margin-left: 700px; margin-top: 30px:">登録</button>
        <div>
             <a href="UserListServlet" style="margin-left: 1000px;" >戻る</a>
        </div>
    </form>
</body>
</html>