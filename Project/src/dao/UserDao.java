package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	public User findByloginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			conn = DBManeger.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			String secret = secret(password);

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, secret);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = DBManeger.getConnection();

			String sql = "SELECT * FROM user WHERE id != 1";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public User findById(String id) {
		Connection conn = null;
		try {
			conn = DBManeger.getConnection();

			String sql = "SELECT * FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			//ここにはなにを書くべきか

			ResultSet rs = pStmt.executeQuery();
			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}
			int idData = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			Date birthData = rs.getDate("birth_date");
			String createData = rs.getString("create_date");
			String updateData = rs.getString("update_date");
			return new User(idData, loginIdData, nameData, birthData, createData, updateData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public User updateUser(String name, String birthDate, String password, String id) {
		Connection conn = null;
		try {
			conn = DBManeger.getConnection();

			String sql = "UPDATE user SET name =?,birth_date = ?,password = ? WHERE id = ?";

			String secret = secret(password);

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, secret);
			pStmt.setString(4, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	public User updateUser(String name, String birthDate, String id) {
		Connection conn = null;
		try {
			conn = DBManeger.getConnection();

			String sql = "UPDATE user SET name =?,birth_date = ? WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	public User deleteUser(String id) {
		Connection conn = null;
		try {
			conn = DBManeger.getConnection();

			String sql = "DELETE FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	public User findByloginInfo(String loginId, String password, String name, String birthDate) {
		Connection conn = null;
		try {
			conn = DBManeger.getConnection();

			String sql = "INSERT INTO user (login_id, password,name, birth_date, create_date, update_date) VALUES (?,?,?,?,now(),now())";

			String secret = secret(password);

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, secret);
			pStmt.setString(3, name);
			pStmt.setString(4, birthDate);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}

	public String secret(String password) {

		String source = password;

		Charset charset = StandardCharsets.UTF_8;

		String algorithm = "MD5";

		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		return result;
	}

	public List<User> findAll1(String loginIdP,String name,String startDate,String endDate) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = DBManeger.getConnection();

			String sql = "SELECT * FROM user WHERE id != 1 ";

			if(!loginIdP.equals("")) {
				sql  += " AND login_id = '" + loginIdP + "'";
			}

			if(!name.equals("")) {
				sql +=" AND name LIKE '%"+ name +"%'";
			}
            if(!startDate.equals("")) {
            	sql +=" AND birth_date >= '"+ startDate +"'";
            }
            if(!endDate.equals("")) {
            	sql +=" AND birth_date <= '"+ endDate +"'";
            }
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				String loginId = rs.getString("login_id");
				String name1 = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");

				User user = new User(loginId, name1, birthDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}
}
